#!/bin/bash

# Recopilación de datos de un API.
wget -c https://datahub.io/core/population/r/population.csv

# Limpieza del conjunto de datos.
cat population.csv | grep 'Spain' | sed 's/,/\t/g' > Spain.tsv

# Visualización del resultado.
gnuplot -e "set terminal dumb; plot 'Spain.tsv' using 3:4"
