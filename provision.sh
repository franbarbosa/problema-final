#!/bin/bash

# Realizaremos las siguientes instalaciones

# Editor de texto: emacs
sudo apt install emacs

# Control de versiones: git
apt-get install git

# Descarga de contenido: wget
apt-get install wget

# Filtrado de datos: grep
apt-get install grep

# Visualización de datos: gnuplot
apt-get install gnuplot
